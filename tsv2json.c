#include <u.h>
#include <libc.h>
#include <bio.h>

static char**
split(char *line, uvlong *len)
{
	char **ret = NULL;
	char *start;
	
	*len = 0;
		
	if (! *line)
		goto ret;
	
	start = line;
	for (; *line; line++) {
		switch (*line) {
		case '\t':
		case '\0':
			*line = '\0';
			ret = realloc(ret, ((*len)+1) * sizeof(char*));
			ret[*len] = start;
			(*len)++;
			line++;
			start = line;
		}
	}
	
	ret:
	return ret;
}

void
main(int argc, char *argv[])
{
	Biobuf bstdin;
	Biobuf bstdout;
	Binit(&bstdin, 0, OREAD);
	Binit(&bstdout, 1, OWRITE);
	char *line, **header, **fields;
	uvlong headerlen, fieldslen, i;
	
	if(! (line = Brdstr(&bstdin, '\n', 1))) {
		exit(0);
	}
	
	header = split(line, &headerlen);
	
	while (line = Brdstr(&bstdin, '\n', 1)) {
		fields = split(line, &fieldslen);
		if (fieldslen != headerlen) {
			fprint(2, "line length doesn't match header\n");
			continue;
		}
		Bputc(&bstdout, '{');
		for (i=1; i<fieldslen; i++)
			Bprint(&bstdout, "%s=%s,", header[i-1], fields[i-1]);
		if (fieldslen>0)
			Bprint(&bstdout, "%s=%s", header[fieldslen-1], fields[fieldslen-1]);
		Bprint(&bstdout, "}\n");
	}
	
	Bflush(&bstdout);
	exit(0);
}
