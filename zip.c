#include <u.h>
#include <libc.h>
#include <bio.h>

Biobuf *bf1, *bf2;
Biobuf bstdout;

void
usage(char *name)
{
	print("%s file1 file2\n", name);
	exit(1);
}

void
done(void)
{
	Bflush(&bstdout);
	exit(0);
}

void
run(char *p1, char *p2)
{
	char *l1, *l2;
	
	bf1 = Bopen(p1, OREAD);
	bf2 = Bopen(p2, OREAD);
	Binit(&bstdout, 1, OWRITE);
	while (l1 = Brdstr(bf1, '\n', 1))
		if (l2 = Brdstr(bf2, '\n', 1))
			Bprint(&bstdout, "%s\t%s\n", l1, l2);
		else
			done();
	done();
}

void
main(int argc, char *argv[])
{
	if (3 == argc)
		run(argv[1], argv[2]);
	else
		usage(argv[0]);
}
