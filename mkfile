MKSHELL=$PLAN9/bin/rc
<$PLAN9/src/mkhdr

%.o: %.c
	$CC -o $target $prereq

%: %.o
	$LD -o $target $prereq

all: select
